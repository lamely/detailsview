package com.example.detailsviewing

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch


@Database(entities = arrayOf(DetailsEntity::class), version = 1, exportSchema = false)
abstract class AppDB : RoomDatabase() {
    abstract fun databaseDAO(): DetailsDao


    private class dataDatabaseCallback(private val scope: CoroutineScope) :
        RoomDatabase.Callback() {
        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            INSTANCE?.let { database ->
                scope.launch {
                    var databaseDao = database.databaseDAO()

                    var data = DetailsEntity(
                        R.id.edit_text_name.toString(),
                        R.id.edit_text_address.toString(),
                        R.id.edit_text_email.toString(),
                        R.id.edit_text_distric.toString(),
                        R.id.edit_text_telephone_number.toString()
                    )
                    databaseDao.Insert(data)
                }.start()

            }
        }
    }

    companion object {
        @Volatile
        private var INSTANCE: AppDB? = null

        fun getDatabase(context: Context, scope: CoroutineScope): AppDB {
            val tempInstance = INSTANCE

            if (tempInstance != null) {
                return tempInstance
            }
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDB::class.java,
                    "database"
                ).addCallback(dataDatabaseCallback(scope)).build()
                INSTANCE = instance
                return instance
            }

        }
    }
}








