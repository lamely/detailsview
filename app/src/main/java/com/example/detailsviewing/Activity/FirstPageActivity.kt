package com.example.detailsviewing.Activity

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.detailsviewing.DetailsEntity
import com.example.detailsviewing.DetailsViewModel
import com.example.detailsviewing.R

class FirstPageActivity : AppCompatActivity() {

    private lateinit var name: EditText
    private lateinit var address: EditText
    private lateinit var email: EditText
    private lateinit var district: EditText
    private lateinit var telno: EditText

    private lateinit var dbViewModel: DetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first_page)

        name = findViewById(R.id.edit_text_name)
        address = findViewById(R.id.edit_text_address)
        email = findViewById(R.id.edit_text_email)
        district = findViewById(R.id.edit_text_distric)
        telno = findViewById(R.id.edit_text_telephone_number)

        val btn_save = findViewById<Button>(R.id.button_save)
        btn_save.setOnClickListener {

            dbViewModel = ViewModelProvider(this).get(DetailsViewModel::class.java)
            Toast.makeText(applicationContext, "Result added", Toast.LENGTH_SHORT).show()

            dbViewModel.insert(
                db = DetailsEntity(
                    name.text.toString(),
                    address.text.toString(),
                    email.text.toString(),
                    district.text.toString(),
                    telno.text.toString()
                )
            )
        }

        val btn_view_all = findViewById<Button>(R.id.button_view_all)
        btn_view_all.setOnClickListener {

            val intent = Intent(this@FirstPageActivity, DataLoadToRecycleviewActivity::class.java)
            startActivity(intent)
        }
    }
}
