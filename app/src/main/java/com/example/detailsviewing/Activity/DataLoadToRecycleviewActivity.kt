package com.example.detailsviewing.Activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.detailsviewing.Adapter.DataListAdapter
import com.example.detailsviewing.Adapter.OnItemClickListner
import com.example.detailsviewing.DetailsEntity
import com.example.detailsviewing.DetailsViewModel
import com.example.detailsviewing.R


class DataLoadToRecycleviewActivity : AppCompatActivity(), OnItemClickListner {

    private  var dbViewModel: DetailsViewModel?=null
    private  var dataList:List<DetailsEntity>?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_load_to_recycleview)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview_item)
        val adapter = DataListAdapter(this, this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        dbViewModel = ViewModelProvider(this).get(DetailsViewModel::class.java)
        dbViewModel?.allData?.observe(this, Observer { data ->
            data?.let {
                dataList=data
                adapter.setDetails(it)
                adapter.notifyDataSetChanged()
            }
        })

    }

    override fun onItemClick(context: Context, position: Int) {
        Toast.makeText(this, "clicked", Toast.LENGTH_SHORT).show()

        val intent = Intent(this, FullDetailsActivity::class.java)

       var position= dataList?.get(position)

        intent.putExtra("name", position?.name)
        intent.putExtra("address",position?.address)
        intent.putExtra("email",position?.email)
        intent.putExtra("district",position?.District)
        intent.putExtra("tel_num",position?.telno)

        startActivity(intent)
    }

}