package com.example.detailsviewing.Activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.detailsviewing.R
import kotlinx.android.synthetic.main.activity_full_details.*

class FullDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_details)

        text_view_view_name.setText(intent.getStringExtra("name"))
        text_view_view_address.setText(intent.getStringExtra("address"))
        text_view_view_email.setText(intent.getStringExtra("email"))
        text_view_view_district.setText(intent.getStringExtra("district"))
        text_view_view_telephone_number.setText(intent.getStringExtra("tel_num"))

    }
}