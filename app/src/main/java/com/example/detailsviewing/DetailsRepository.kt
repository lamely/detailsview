package com.example.detailsviewing

import androidx.lifecycle.LiveData

class DetailsRepository(private val detailsDao: DetailsDao) {
    val allDetails: LiveData<List<DetailsEntity>> = detailsDao.getAll()

    suspend fun insert(details: DetailsEntity) {
        detailsDao.Insert(details)
    }

}