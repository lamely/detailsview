package com.example.detailsviewing

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface DetailsDao {

    @Query("SELECT * FROM details")
    fun getAll(): LiveData<List<DetailsEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun Insert(data: DetailsEntity)
}