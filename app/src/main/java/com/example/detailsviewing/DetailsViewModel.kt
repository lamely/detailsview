package com.example.detailsviewing

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch


class DetailsViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: DetailsRepository

    val allData: LiveData<List<DetailsEntity>>

    init {
        val databaseDao = AppDB.getDatabase(application, viewModelScope).databaseDAO()
        repository = DetailsRepository(databaseDao)
        allData = repository.allDetails
    }

    fun insert(db: DetailsEntity) = viewModelScope.launch {
        repository.insert(db)

    }
}