package com.example.detailsviewing.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.detailsviewing.DetailsEntity
import com.example.detailsviewing.R

class DataListAdapter internal constructor(private val context: Context,var clickListner:OnItemClickListner) :
    RecyclerView.Adapter<DataListAdapter.databaseViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var details = emptyList<DetailsEntity>()

    inner class databaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val databaseItemView_name: TextView = itemView.findViewById(R.id.text_view_dis_name)
        val databaseItemView_address: TextView = itemView.findViewById(R.id.text_view_dis_address)
        val databaseItemView_email: TextView = itemView.findViewById(R.id.text_view_dis_email)
        val databaseItemView_district: TextView = itemView.findViewById(R.id.text_view_dis_district)
        val databaseItemView_telno: TextView = itemView.findViewById(R.id.text_view_dis_tel_num)

        var name=itemView.findViewById<TextView>(R.id.text_view_view_name)

        fun initialized(context: Context,action:OnItemClickListner){
           databaseItemView_name.text

            itemView.setOnClickListener {
                action.onItemClick(context,adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): databaseViewHolder {
        val itemView = inflater.inflate(R.layout.activity_details_cardview, parent, false)
        return databaseViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: databaseViewHolder, position: Int) {
        val current = details[position]
        holder.databaseItemView_name.text = current.name
        holder.databaseItemView_address.text = current.address
        holder.databaseItemView_email.text = current.email
        holder.databaseItemView_district.text = current.District
        holder.databaseItemView_telno.text = current.telno

         holder.initialized(context,clickListner)

    }
    internal fun setDetails(data: List<DetailsEntity>) {
        this.details = data
        notifyDataSetChanged()
    }

    override fun getItemCount() = details.size

}

interface OnItemClickListner{
    fun onItemClick( context: Context,position: Int) {
    }
}
